    function ResetMovement() {
	    $( "#golem" ).css( "transform", "scaleX(1)" );
        $( "#golem_second" ).css( "transform", "scaleX(1)" );
        $( "#golem_third" ).css( "transform", "scaleX(1)" );
	    $( "#cultist_one" ).css( "transform", "scaleX(1)" );
		$( "#cultist_two" ).css( "transform", "scaleX(1)" );
		$( ".golem_optional" ).css( "display", "none" );   

        $( "#cultist_one" ).css( "left", parseFloat( -300 ) );
		$( "#cultist_two" ).css( "left", parseFloat( -230 ) );	
		$( "#golem" ).css( "left", parseFloat( -10 ) );
        $( "#golem_second" ).css( "left", parseFloat( 20 ) );	    
        $( "#golem_third" ).css( "left", parseFloat( 50 ) );   
    } 

    $( document ).ready(function(){   

		var nVariation = 0;
		var nDir = 1;

		setInterval(function(){
			if ( ( parseFloat( $( "#cultist_one" ).css( "right" ) ) < -150 ) && ( nDir == 1 ) )	{
				nDir = -1;
				$( "#golem" ).css( "transform", "scaleX(-1)" );
                $( "#golem_second" ).css( "transform", "scaleX(-1)" );
                $( "#golem_third" ).css( "transform", "scaleX(-1)" );
				$( "#cultist_one" ).css( "transform", "scaleX(-1)" );
				$( "#cultist_two" ).css( "transform", "scaleX(-1)" );
				$( ".golem_optional" ).css( "display", "block" );

			} else if ( ( parseFloat( $( "#cultist_one" ).css( "left" ) ) < -650 ) && ( nDir == -1 ) )	{
				nDir = 1;
				$( "#golem" ).css( "transform", "scaleX(1)" );
                $( "#golem_second" ).css( "transform", "scaleX(1)" );
                $( "#golem_third" ).css( "transform", "scaleX(1)" );
				$( "#cultist_one" ).css( "transform", "scaleX(1)" );
				$( "#cultist_two" ).css( "transform", "scaleX(1)" );
				$( ".golem_optional" ).css( "display", "none" );
			}			

       		nVariation = 5.5 * nDir;
			$( "#cultist_one" ).css( "left", parseFloat( $( "#cultist_one" ).css( "left" ) ) + nVariation );
			$( "#cultist_two" ).css( "left", parseFloat( $( "#cultist_two" ).css( "left" ) ) + nVariation );	
			$( "#golem" ).css( "left", parseFloat( $( "#golem" ).css( "left" ) ) + nVariation );
            $( "#golem_second" ).css( "left", parseFloat( $( "#golem_second" ).css( "left" ) ) + nVariation );	    
            $( "#golem_third" ).css( "left", parseFloat( $( "#golem_third" ).css( "left" ) ) + nVariation );          
			
		}, 35);

	

    });
