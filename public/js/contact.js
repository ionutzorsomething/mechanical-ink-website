    $( document ).ready(function(){   

		var nRotation = 0;
		var nDirection = 1;

		jQuery.fn.rotate = function( nDegrees ) {
    			$( this ).css({ 'transform' : 'rotate('+ nDegrees +'deg)'});
		        return $( this );
		};

		$( '#middle_pieces' ).on( 'mouseenter', function() {
    			this.iid = setInterval(function() {
       				nRotation += 3;
 	 	  		$(  '#middle_pieces' ).rotate( nRotation );      
    			}, 25);
		
		}).on( 'mouseleave', function()	{
 			this.iid && clearInterval(this.iid);
		});

		$( '#pieces_2' ).on( 'mouseenter', function() {
    			this.iid = setInterval(function() {
       				nRotation += 5;
 	 	  		$(  '#middle_pieces' ).rotate( nRotation );      
    			}, 7);
		
		}).on( 'mouseleave', function()	{
 			this.iid && clearInterval(this.iid);
		});

		$( '#pieces_1' ).on( 'mouseenter', function() {
    			this.iid = setInterval(function() {
				    if (
                         ( ( parseInt( $( "#middle_pieces" ).css( "height" ) ) > 80 ) && ( nDirection == 1 ) ) ||
                         ( ( parseInt( $( "#middle_pieces" ).css( "height" ) ) < 10 ) && ( nDirection == -1 ) )
                       ) {
					    nDirection *= -1;	
					    $( '#middle_pieces' ).stop( true, true ).animate();		
				    }

				    if ( nDirection == 1 )	{
				     	$( '#middle_pieces' ).animate( { height: '+=1px' }, 40 );
				    } else if ( nDirection == -1 )	{
					    $( '#middle_pieces' ).animate( { height: '-=1px' }, 40 );
				    }      
    			}, 25);		

		}).on( 'mouseleave', function()	{
			$( '#middle_pieces' ).stop( true, true ).animate();
 			this.iid && clearInterval(this.iid);
		});

    });
