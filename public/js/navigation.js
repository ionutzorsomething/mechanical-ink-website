   function ChangePage( address ) {

	    $( ".body_type" ).css( "display", "none" );
	
	    if ( address == "main" ) {	
		    $( "#games_body" ).css( "display", "inline-block" );
	    } else if ( address == "contact" ) {	
		    $( "#contact_body" ).css( "display", "block" );
	    } else if ( address == "follow" ) {
		    $( "#follow_body" ).css( "display", "block" );

		    // reset
		    $( "#cultist_one" ).css( "left", "-300" );	
		    $( "#cultist_two" ).css( "left", "-230" );
		    $( "#golem" ).css( "left", "-10" );

		    //$( ".golem_optional" ).css( "visibility", "hidden" );
	    } else if ( address == "news" ) {
		    $( "#news_body" ).css( "display", "block" );
	    }

    }

    $( document ).ready(function(){    
	$( "#goto_main" ).on( "click", function() {
		ChangePage( "main" );
	}); 

	$( "#goto_contact" ).on( "click", function() {
		ChangePage( "contact" );
	}); 

	$( "#goto_follow" ).on( "click", function() {
        ResetMovement();
		ChangePage( "follow" );
	});  

	$( "#goto_news" ).on( "click", function() {
		ChangePage( "news" );
	});  


    });
